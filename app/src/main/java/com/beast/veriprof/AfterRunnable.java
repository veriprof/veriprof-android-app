package com.beast.veriprof;

/**
 * Created by griffins on 5/2/16.
 */
public abstract class AfterRunnable {
    public abstract void run(Object result);
}
