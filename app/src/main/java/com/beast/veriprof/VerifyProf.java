package com.beast.veriprof;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.FileAsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;


public class VerifyProf extends AppCompatActivity {
    private String code;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_credit);
        code = getIntent().getStringExtra("code");
        setTitle("Loading details for: " + code);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        loadDetails(code);
    }

    private void loadDetails(String code) {
        String ip = "http://192.168.43.50/credits/public/api/";
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("pract", code);
        client.get(ip + "all", params, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                TextView text = (TextView) findViewById(R.id.info);
                text.setVisibility(View.VISIBLE);
                try {
                    setTitle(response.getString("surname") + ", " + response.getString("other_names"));
                    String data = "CREDITS\n\n";
                    String photo = response.getString("photo");
                    loadPhoto(photo);
                    JSONArray credits = response.getJSONArray("credits");
                    for (int x = 0; x < credits.length(); x++) {
                        JSONObject credit = credits.getJSONObject(x);
                        data += credit.getString("credit");
                        data += "\n";
                        data += credit.getString("year");
                        data += "\n";
                        data += credit.getString("institution");
                        data += "\n\n";
                    }
                    data += "\n\n\n";
                    data += "PRACTICE HISTORIES\n\n";

                    JSONArray practise = response.getJSONArray("practice");
                    for (int x = 0; x < practise.length(); x++) {
                        JSONObject practice = practise.getJSONObject(x);
                        data += practice.getString("place");
                        data += "\n";
                        data += practice.getString("role");
                        data += "\n";
                        data += practice.getString("address");
                        data += "\n";
                        data += "From: " + practice.getString("from")
                                + " To: " + (practice.getString("to").contentEquals("null") ?
                                "Current" : practice.getString("to"));
                        data += "\n\n";
                    }

                    text.setText(data);
                } catch (JSONException e) {
                    Snackbar.make(findViewById(R.id.container), "Couldn't decode retrieve data from server", Snackbar.LENGTH_INDEFINITE).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Snackbar.make(findViewById(R.id.container), "Couldn't retrieve data from server", Snackbar.LENGTH_INDEFINITE).show();
            }

            @Override
            public void onFinish() {
                super.onFinish();
                findViewById(R.id.progressbar).setVisibility(View.GONE);
            }
        });
    }

    private void loadPhoto(String photo) {
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(this, photo, new FileAsyncHttpResponseHandler( new File(VerifyProf.this.getFilesDir().getPath().concat("/photo.jpg"))) {
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, File photo) {
                final ImageView pic = (ImageView) findViewById(R.id.pic);
                Utils utils = new Utils(VerifyProf.this);
                utils.LoadImage(photo.getAbsolutePath(), 340, new AfterRunnable() {
                    @Override
                    public void run(Object result) {
                        pic.setImageBitmap((Bitmap) result);
                    }
                });
            }
        });
    }
}
